# Minecraft

> The configuration for my Minecraft server.

## Introduction

This repository contains the configuration for my [Minecraft](https://minecraft.net/) server.

It uses the [itzg/minecraft-server](https://github.com/itzg/docker-minecraft-server) [Docker](https://docker.com/) image, with [itzg/mc-router](https://github.com/itzg/mc-router) for subdomain handling.

## Usage

Start the server with [Docker Compose](https://docs.docker.com/compose/).

```sh
docker-compose up --detach
```

## Mod List

The server loads the following mods:

- [AntiXray](https://modrinth.com/mod/anti-xray)
- [Fabric API](https://modrinth.com/mod/fabric-api)
- [Lithium](https://modrinth.com/mod/lithium)
- [No Chat Reports](https://modrinth.com/mod/no-chat-reports)
- [No Telemetry](https://modrinth.com/mod/no-telemetry)
- [Servux](https://curseforge.com/minecraft/mc-mods/servux)
- [Starlight](https://modrinth.com/mod/starlight)
